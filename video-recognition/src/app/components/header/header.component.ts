import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  status: boolean = false;
  
  constructor(
    private router: Router,
    public translate: TranslateService,
  ) {
    translate.addLangs(['en', 'de']);
    translate.setDefaultLang('en');
   }

  ngOnInit() {
  }

  switchLang(lang: string) {
    this.translate.use(lang);
  }

  clickEvent(){
      this.status = !this.status;       
  }

  logout(){
    this.router.navigateByUrl('login');
    localStorage.clear();
  }
}
