import { Component, OnInit } from '@angular/core';
import { CameraService } from './camera.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit {

  cameraOne = [];
  cameraTwo = [];

  constructor(
    private cameraService: CameraService,
    public translate: TranslateService,
  ) { 
    translate.addLangs(['en', 'de']);
    translate.setDefaultLang('en');
  }

  switchLang(lang: string) {
    this.translate.use(lang);
  }

  ngOnInit() {
    let i = 0;
    setInterval (() => {
      this.getCameraOutput(i);
      if( i == 1) {
        i = 0;
      }else{
        i= 1;
      }
    }, 500);
  }

  getCameraOutput(i){
    this.cameraService.getCameraOutput(i).subscribe((res: any) => {
      if (res.status === 200) {
        this.cameraOne = res.body.cameraOne;
        this.cameraTwo= res.body.cameraTwo; 
      } else {
        console.log('something went wrong');
      }
    });
  }

  getClass(result){
    if(result == 'pass'){
      return 'fa fa-thumbs-up';
    }else{
      return 'fa fa-thumbs-down';
    }
  }

  
}
