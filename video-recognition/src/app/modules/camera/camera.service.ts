import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CameraService {

  constructor(private http: HttpClient) { }
  getCameraOutput(i): any {
      if(i == 0){
        var url = 'https://run.mocky.io/v3/6235d7ba-5538-4aa3-94ea-512c62ef72db';
        i = i++
      }else{
        var url = 'https://run.mocky.io/v3/c2b738e4-5520-4a2e-9f49-4af887f3ac69';
        i = 0;
      }
    

    return this.http.get(url, { observe: 'response' })
    .pipe(map(res => {
        if (res.status == 200) {
          return res;
        }
    }));
  }

}
