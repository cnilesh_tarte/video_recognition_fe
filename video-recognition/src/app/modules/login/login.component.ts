import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  status: boolean = false;
  constructor(
    public translate: TranslateService,
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    translate.addLangs(['en', 'de']);
    translate.setDefaultLang('en');
  }

  switchLang(lang: string) {
    this.translate.use(lang);
  }

  ngOnInit() {
  }

  validateUser(){
    if(this.loginForm.valid){
      let username = this.loginForm.controls['username'].value;
      let password = this.loginForm.controls['username'].value;
      this.loginService.loginUser(this.loginForm.value).subscribe((res: any) => {
        if (res.status === 200) {
            let response = res.body.data;
            if(response.username === username && response.password === password){
              this.status = false;       
              localStorage.setItem('username', response.username);
              this.router.navigateByUrl('camera');
            }else{
              this.status = true;       
            }
        } else {
          console.log('something went wrong');
        }
      });
    }
  }

}
