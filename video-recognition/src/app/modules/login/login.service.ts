import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  loginUser(loginData): any {
    return this.http.post('https://run.mocky.io/v3/f093c9bd-8745-4edc-a9c4-5432d33f9139', loginData, { observe: 'response' })
    .pipe(map(res => {
        if (res.status == 200) {
          return res;
        }
    }));
  }
}
