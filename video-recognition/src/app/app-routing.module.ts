import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './modules/login/login.module#LoginModule' },
  { path: 'camera', loadChildren: './modules/camera/camera.module#CameraModule' },
  { path: 'report', loadChildren: './modules/report/report.module#ReportModule' },
  { path: 'settings', loadChildren: './modules/settings/settings.module#SettingsModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
